FROM php:7.0-fpm-alpine

COPY . /app
WORKDIR /app

RUN apk add --update nginx && rm -rf /var/cache/apk/*

RUN adduser -D -g 'www' www

RUN chown -R www:www /var/lib/nginx
RUN chown -R www:www /app

COPY nginx.conf /etc/nginx/nginx.conf
# COPY nginx/default.conf /etc/nginx/conf.d/default.conf

EXPOSE 8080

ENTRYPOINT ["entrypoint.sh"]
